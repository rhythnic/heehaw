# Heehaw

Heehaw is a tiny event emitter based on the [Node.js EventEmitter][events] but not feature complete.

## Deno import

- **latest** - `import { EventEmitter } from "https://gitlab.com/rhythnic/heehaw/-/raw/master/src/mod.ts"`
- **versioned** - `import { EventEmitter } from "https://gitlab.com/rhythnic/heehaw/-/raw/VERSION/src/mod.ts"`

## NPM install

`npm install heehaw`

## Usage

```
import { EventEmitter } from 'heehaw'

const events = new EventEmitter()

function handler (...args) {
  console.log(args.join(' '))
}

events.on('hello', handler)
events.emit('hello', 'Hello', 'World')

events.off('hello', handler)

// remove all listeners
events.listeners('hello').forEach(x => events.off('hello', x))

```

## API

### on(topic, handler)

- topic [String][string]
- handler [Function][function]

### once(topic, handler)

- topic [String][string]
- handler [Function][function]

### off(topic, handler)

- topic [String][string]
- handler [Function][function]

### emit(topic[, ...args])

- topic [String][string]

Any exceptions or rejected promises that come from a handler will cause an "error" event to be emitted.

### listeners(topic)

- topic [String][string]
- returns an [Array][array] of handler functions

## LICENSE

MIT license, Copyright Nicholas Baroni 2020

[string]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String
[function]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function
[array]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array
[events]: https://nodejs.org/api/events.html
