import { EventEmitter } from "../src/mod.ts";
import { assertEquals, assertStrictEquals } from "./deps.ts";
import { spy, asyncRunner } from "./test-utils.js";

const setup = (spyFn) => {
  return { events: new EventEmitter(), handler: spy(spyFn) };
};

Deno.test("listen to a topic", () => {
  const { events, handler } = setup();
  events.on("a", handler);
  events.emit("a", 0, 1);
  assertEquals(handler.calls[0], [0, 1]);
});
Deno.test("can listen once", () => {
  const { events, handler } = setup();
  events.once("a", handler);
  events.emit("a", 0);
  events.emit("a", 1);
  assertStrictEquals(handler.calls.length, 1);
});
Deno.test("unsubscribe from topic", () => {
  const { events, handler } = setup();
  events.on("a", handler);
  events.emit("a");
  events.off("a", handler);
  events.emit("a");
  assertStrictEquals(handler.calls.length, 1);
});
Deno.test("get all listeners for a topic", () => {
  const { events, handler } = setup();
  events.on("a", handler);
  assertStrictEquals(events.listeners("a")[0], handler);
});
Deno.test(
  "emits an error if a handler throws",
  asyncRunner((resolve, reject) => {
    const error = new Error("TEST");
    const events = new EventEmitter();
    const handler = spy(async () => {
      throw error;
    });
    events.on("a", handler);
    events.on("error", (err) => {
      try {
        assertStrictEquals(err, error);
        resolve();
      } catch (err) {
        reject(err);
      }
    });
    events.emit("a");
  }),
);
Deno.test(
  "emits an error if a handler rejects",
  asyncRunner((resolve, reject) => {
    const error = new Error("TEST");
    const events = new EventEmitter();
    const handler = spy(async () => {
      throw error;
    });
    events.on("a", handler);
    events.on("error", (err) => {
      try {
        assertStrictEquals(err, error);
        resolve();
      } catch (err) {
        reject(err);
      }
    });
    events.emit("a");
  }),
);
