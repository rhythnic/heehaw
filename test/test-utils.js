export function spy(fn) {
  const _stub = (...args) => {
    _stub.calls.push(args);
    return !fn ? void 0 : fn(...args);
  };
  _stub.calls = [];
  return _stub;
}

export function asyncRunner(fn) {
  return () =>
    new Promise((resolve, reject) => {
      try {
        fn(resolve, reject);
      } catch (error) {
        reject(error);
      }
    });
}
