runtest:
	- docker build -f ./build/Dockerfile.test -t heehawtest:latest .
	- docker create --name heehawtest heehawtest:latest
	- docker run heehawtest lint --unstable
	- docker run heehawtest
	- docker rm heehawtest
transpile:
	- docker build -f ./build/Dockerfile.npm -t heehawnpm:latest .
	- docker create --name heehawnpm heehawnpm:latest
	- docker cp heehawnpm:/app/lib ./lib
	- docker rm heehawnpm
clean:
	- docker image rm heehawtest:latest heehawnpm:latest