export class EventEmitter {
  _topics: Map<string, Function[]>;
  constructor() {
    this._topics = new Map();
  }
  listeners(topic: string): Function[] {
    return this._topics.get(topic) || [];
  }
  on(topic: string, handler: Function): EventEmitter {
    this._topics.set(topic, [...this.listeners(topic), handler]);
    return this;
  }
  once(topic: string, handler: Function): EventEmitter {
    const _handler = (...args: unknown[]): void => {
      handler(...args);
      this.off(topic, _handler);
    };
    this.on(topic, _handler);
    return this;
  }
  off(topic: string, handler: Function): EventEmitter {
    this._topics.set(
      topic,
      this.listeners(topic).filter((x) => x !== handler)
    );
    return this;
  }
  emit(topic: string, ...args: unknown[]): EventEmitter {
    this.listeners(topic).forEach(this.callListener(topic, args));
    return this;
  }
  private callListener(
    topic: string,
    args: unknown[]
  ): (listener: Function) => void {
    return (listener) => {
      let result;
      try {
        result = listener(...args);
      } catch (error) {
        if (topic !== "error") this.emit("error", error);
      }
      if (result instanceof Promise) {
        return result.catch((err) => {
          if (topic !== "error") this.emit("error", err);
        });
      }
    };
  }
}
